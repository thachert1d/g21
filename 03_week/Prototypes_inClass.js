var minionOne = {};
var minionTwo = {};

// Object {}
minionOne.constructor()

minionOne = {
  sayBanana: function() {
    return "banana";
  }
};

minionTwo = {
  sayBanana: function() {
    return "banana";
  }
};

// "banana"
minionOne.sayBanana();

// "banana"
minionTwo.sayBanana();

Object.prototype.sayBanana = function() {
  return "banana";
};

// "banana"
minionOne.sayBanana();

// "banana"
minionTwo.sayBanana();

var gollum = {};

// "banana"
gollum.sayBanana();

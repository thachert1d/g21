//IN TERMINAL
// Create directory
//Move into project folder
//npm init
// touch server.js
// touch .eslint.js
// npm install -- save express
// echo 'node_modules' >> .gitignore
// npm install -- save body-parser
// npm install -- save morgan

//TO TEST
// new TERMINAL
  // run nodemon
  // view in browser at http://localhost:5000/pets OR
  // view in terminal by running command: http GET localhost:5000/pets (without authentication) OR http http://guest:password@localhost:5000/pets (with authentication where username: guest and password: password)

// Dummy file setup
  //Basic Header/Up-Top
'use strict';

const fs = require('fs');
const path = require('path');
const petsPath = path.join(__dirname, 'pets.json');

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());

const morgan = require('morgan');
app.use(morgan('short'));

  //for authorization ( this route goes at the top, does not go within the readFile)
  var basicAuth = require('basic-auth');

  var auth = function (req, res, next) {
    function unauthorized(res) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
      return unauthorized(res);
    };

    if (user.name === 'guest' && user.pass === 'password') {
      return next();
    } else {
      return unauthorized(res);
    };
  };

  //when using a readFile everything goes within that function because everything is dependent on reading the file

  //READING DATA FROM A FILE
fs.readFile(petsPath, 'utf8', (err, data) => {
  if (err) {
    return next(err);
  } else { //EVERYTHING ELSE GOES HERE

  //TYPES OF ROUTES ARE //
app.use((req, res) => {});
app.get((req, res) => {});
app.post((req, res) => {});
app.put((req, res) => {});
app.delete((req, res) => {});
app.patch((req, res) => {});
app.listen(PORT, () => {});

  // SIMPLE/GENERAL ROUTE FORMAT (including setHeader and statusCode)
  // 'auth,' only used if authentication is being used (just remove if not using)
  // 'next' ony used when when it fits with error handling or into the event handling flow (just remove if not using)
app.get('/pets/', auth, (req, res, next) => {
res.setHeader('Content-Type', 'application/json');
res.statusCode = 200;
res.send(pets);
});

  //WRITING BACK TO A FILE (still goes within the read file and then INSIDE of each function that requires writing to the file, POST, PUT, PATCH, etc.)
fs.writeFile(petsPath, petsString, (err, data) => {
  if (err) {
    return next(err); // using this instead of 'throw err;' uses the app.use() 500 error module
  } else {
    res.send(petsString);
  }
});

  //these routes catch and address different kinds of errors
app.use((req, res, next) => {
  res.status(404).send('Sorry cant find that!');
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

  // This goes at the very end and runs everything on your local host as indicated
app.listen(5000, () => {
  console.log('Go to localhost:5000/');
});

// requires Node's `http` module
var http = require('http');

//declares a function that gets invoked on every request
function handleRequest(req, res) {
  // res.setHeader("Content-Type", "text/plain");
  // res.end("Hi, I'll take two packets of sugar");
  if(req.url === '/special-message') {
    res.end("You're special");
  } else if (req.url ===
  '/non-special-message') {
    res.end("You're boring!");
  } else {
    res.end(req.url);
  }
};

//Creates an instance of a server with our callback
var server = http.createServer(handleRequest);

//Binds our server to a port, host, and then logs a message
server.listen(8080, 'localhost', function() {
  console.log("Listening...");
});

//////NOW WITH EXPORT MODULE

var http = require('http');
var routes = require('./routes');

var handleRequest = function(req, res) {
  if (routes[req.url]) {
    routes[req.url](req, res);
  } else {
    res.end("404, no such route");
  }
};

var server = http.createServer(handleRequest);

server.listen(8080, function() {
  console.log("Listening...");
});

var app = angular.module("madlibsApp", []);
app.controller("MadlibsController", function($scope){
  $scope.boysName = "Michael";
  $scope.adjective = "happy";
  $scope.pluralNoun = "runs";
  $scope.pluralNoun1 = "tables";
  $scope.insectPlural = "ants";
  $scope.pluralNoun2 = "hits";
  $scope.verbEndingInS = "shuffles";
})

####1. Intro and Setup

* Why learn Angular JS over other frameworks like Ember, Backbone, Knockout, etc?
  * Angular is supported by Google and has a much stronger community and  therefore help available through youTube, Stack Overflow, etc. It is also very robust in comparison, however is the smallest of the three when including required dependencies
* People have some very strong opinions about Angular. What are 3 common complaints people have about Angular?
  * directives API is confusing, particularly transclusion
  * prototypal inheritance is confusing when coming from object oriented languages
  * complicated logic in the view layer can make it difficult to test and find bugs

* Is Angular an MVC framework?
  * MVC = Model-View-Controller
    * the model is the data, the view is the UI, the controller is the business logic
  * Technically, but it is actually MVVM, Model-View-ViewModel, which evolved as a way to separate logical units and concerns when developing large applications (dividing the application into the distinct modular parts listed above)

* Turn to the Angular docs. Find ng-app. What is it and what does it do? What does ng stand for?
  * ng-app is the directive to auto-bootstrap an angular js application, designating the root element of the application
  * the ng stands for angular

####2. Data Binding
* What does ng-model do?
  * ng-model binds an input, select, textarea or custom form control to a property on the scope
  * where the data lives ($scope.name -- followed by $scope.name.title, $scope.name.whateverwhatever)
  * breakdown example: https://github.com/gSchool/angular-curriculum/blob/master/Unit-1/03-angular-mvc.md

* What is "dirty checking"?
  * relatively efficient approach to checking for changes on a model, checks for consistency

* Find a way to set the initial value of "name" as "BoJack" (without writing a controller).
  * ng-init="name= 'BoJack'">

* What are those {{ }} expressions? Are they Handlebars?
  * the {{ X }} indicate a binding, tells angular to evaluate an expression and insert the result into the DOM in place of the binding

* Explain what two-way data binding is.
  * any changes to the model or the view are immediately reflected in both places

* BONUS: Research the $digest loop


####3. Angular MVC
  * No questions (see slides)

####4. Expressions and Filters
  * What are Angular expressions? How do they compare to tags from templating engines you've used before?
    * javascript-like code snippets that are placed in bindings, but also used directly in directive attributes
    * similar to javascript
  * What happens when you write invalid code in an expression? What type of error do you get?
    **
  * What are Angular filters? Describe what a filter does and then name four built-in filters, including one that we haven't used yet.
    * Angular Filters format the value of an expression for display
    * Some filters include: number, date, json, limitTo, orderBy
  * What's the syntax for filters?
    {{ expression | filter }}
  * Can you use more than one filter? yes.
    {{ expression | filter1 | filter2 }}
  * What is a use case for a custom filter?
    * case/client specific needs

####5. Built-In Directives
* What is the purpose of ng-init?
  * a directive allows you to evaluate an expression in the current scope

* Why use ng-src and ng-href?
  * ng-src addresses the issue of using {{}} in a src attribute
  <img ng-src="http://www.gravatar.com/avatar/{{hash}}" alt="Description" />
  * ng-href addresses the issue of using {{}} in a url
  <a ng-href="http://www.gravatar.com/avatar/{{hash}}">link1</a>

* What are directives?
  * directives are angular's way of extending HTML, creating dynamic components that re-render whenever the underlying data changes

* Does ng-class require an object to be passed in?
  an expression is passed in, and can be a string or an object

* What order does an ng-repeat display items in?

* How does ng-repeat handle duplicate data?
  using the index property duplicates can be ignored

* ng-cloak - hides the {{handlebar terms}} when the page loads/reloads
* ng-include - fetches, compiles, and includes an external HTML fragment
* ng-pluralize - given different options (0,1,2) you list different options in order to account for singular/plural terms

####6. Intro to Controllers
What is $scope?
What are Angular modules? What's the syntax for defining a module?
Why do we pass in $scope as an argument to controller functions?
In Express, what are Angular controllers most analogous to?

####7. Scope
What is $rootScope?
Explain how $scope is passed from a parent to child controller
List five built in directives that create their own scope
"Scope becomes tricky when you try to 2 way data bind to a primitive defined on the parent scope from inside the child scope" - what does this mean?

####8. Angular Events

####9. Form Validation

####10. Animations

####11. Unit 1 Assessment Reddit Clone

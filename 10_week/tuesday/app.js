// var rp = require('request-promise');
// var url = process.argv[2] || 'https://fs-student-roster.herokuapp.com/'
//
// rp({uri: url})
//   .then(function(data) {
//     console.log(data);
//   })
//   .catch(function(err) {
//     console.log(err);
//   });

// 'use strict'
//
// var rp = require('request-promise');
// var imdbId = process.argv[2];
//
//
// rp({uri: `http://www.omdbapi.com/?i=${imdbId}`, json: true})
//   .then(data => {
//     let movie = {
//       title: data.Title,
//       year: data.Year,
//       actors: data.Actors,
//       genre: data.Genre,
//       length: data.Runtime
//     }
//
//     console.log(movie);
//   })
//   .catch(function(err) {
//     console.log(err);
//   });

'use strict'
var rp = require('request-promise');
var longUrl = process.argv[2];
var key = 'AIzaSyC5qY003g2sfws4Fzc9lL264EM7Nks3AGk';
var data = {longUrl};

var options = {
  uri: 'https://www.googleapis.com/urlshortener/v1/url/?key=' + key,
  method: 'POST',
  json: true,
  body: data
};

rp(options)
  .then(function(parsedBody) {
    console.log(parsedBody);
  })
  .catch(function(err) {
    console.log(err);
  });

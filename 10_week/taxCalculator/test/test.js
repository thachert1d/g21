const code = require ('../main');
const expect = require ('chai').expect;

describe("taxCalc", () => {
  it("should return 10% of income if when income is less than or equal to $10", () => {
    expect(code.taxCalc(9)).to.equal(.9);
    expect(code.taxCalc(1)).to.equal(.1);
    expect(code.taxCalc(5)).to.equal(.5);


  });
});

describe("taxCalc", () => {
  it("should return 1 + 7% of the amount over $10 when income is greater than $10 and equal to or less than $20", () => {
    expect(code.taxCalc(20)).to.equal(1.70);
    expect(code.taxCalc(18)).to.equal(1.56);

  });
});

describe("taxCalc", () => {
  it("should return 1.7 + 5% of the amount over $20 when income is greater than $20 and equal to or less than $30", () => {
    expect(code.taxCalc(30)).to.equal(2.20);
    expect(code.taxCalc(27)).to.equal(2.05);

  });
});

describe("taxCalc", () => {
  it("should return 2.2 + 3% of the amount over $30 when income is greater than $30", () => {
    expect(code.taxCalc(40)).to.equal(2.50);
    // expect(code.taxCalc(50)).to.equal(2.80);
    expect(code.taxCalc(740)).to.equal(23.5);

  });
});

module.exports = {
  taxCalc: (input) => {
    if(input <= 10) {
      // tax = (input * .1);
      return (input * .1);
    } else if (input <= 20) {
      return (1 + ((input-10)*7)/100);
    } else if (input <= 30) {
      return (1.7 + ((input-20)*5)/100);

    } else if (input > 30) {
      return (2.2 + ((input-30)*3)/100);
    } 
    else {
      return false;
    }
  }
}

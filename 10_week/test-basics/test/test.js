const code = require('../main');
const expect = require('chai').expect;

// describe('', () => {
//   it('');
// });

describe("Hello World", () => {
  it("should say 'Hello, World!' when ran", () => {
    expect(code.helloWorld()).to.equal('Hello, World!');
  });
});

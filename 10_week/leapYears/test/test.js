const code = require('../main');
const expect = require('chai').expect;

describe("leap", () => {
  it("should return true if divisible by 4", () => {
    expect(code.leap(4)).to.equal(true);
    expect(code.leap(8)).to.equal(true);
    expect(code.leap(7)).to.equal(false);
  });
});

describe("leap", () => {
  it("should return false if divisible by 100 and not by 400", () => {
    expect(code.leap(4)).to.equal(true);
    expect(code.leap(500)).to.equal(false);
  });
});

describe("leap", () => {
  it("should return false if year is 1700, 1800, 1900", () => {
    expect(code.leap(2000)).to.equal(true);
    expect(code.leap(1600)).to.equal(true);
    expect(code.leap(1700)).to.equal(false);
    expect(code.leap(1800)).to.equal(false);
    expect(code.leap(1900)).to.equal(false);

  });
});

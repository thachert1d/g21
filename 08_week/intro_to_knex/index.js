var knex = require('./db/knex');
// Select all
// knex('movies').select().then(function(data){
//   console.log(data);
//   process.exit(1);
// })

// Select not all
// knex('movies').select(['title', 'description']).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

//adding conditions to the request
// knex('movies').select().where('rating', '>', 4).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

// multiple where clauses
// knex('movies').select().where('rating', '>', 4).where({title: 'Cars'}).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

//OR clauses
// knex('movies').select().where({title: 'Cars'}).orWhere({title: 'Gigli'}).then(function(data){
//   console.log(data);
//   process.exit(1);
// });

// ORDER BY / LIMITS
// knex('movies').select().orderBy('rating', 'desc').limit(5).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

//Exercises
// knex('movies').select().orderBy('rating', 'ASC').limit(1).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

// knex('movies').select().where({title: 'Gigli'}).orWhere({title: 'Mad Max: Fury Road'}).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

// knex('movies').select(['id', 'title']).limit(5).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

// knex('movies').select().where('rating', '<', 8).andWhere('rating', '>', 4).then(function(data){
//   console.log(data);
//   process.exit(1);
// })

//Updating

// knex('movies').where({title: 'Gigli'}).update({rating: 10}).then(function() {
//   knex('movies').select().where({title: 'Gigli'}).then(function(data) {
//     console.log(data);
//     process.exit(1);
//   });
// });

// Update without loop
// knex('movies').where({title: 'Gigli'}).update({rating: 10}, '*').then(function(data) {
//     console.log(data);
//     process.exit(1);
//   });

// del
// knex('movies').where({title: 'Gigli'}).del().then(function() {
//   knex('movies').select().where({title: 'Gigli'}).then(function(data) {
//     console.log(data);
//     process.exit(1);
//   });
// });

// insert
// knex('movies').insert({title: 'Gigli', description: 'Best movie evar', rating: 10}, '*').then(function(data) {
//   console.log(data);
//   process.exit(1);
// });
//
//   //OR
// knex('movies').insert({title: 'Gigli', description: 'Best movie evar', rating: 10}).then(function(data) {
//   console.log(data);
//   process.exit(1);
// });

// Include dependency: ngCordova
angular.module('starter', ['ionic', 'ngCordova'])

.run(function($ionicPlatform, $cordovaSQLite) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        console.log('ready');

        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          StatusBar.styleDefault();
        }

        // Important!!
        //
        // Instantiate database file/connection after ionic platform is ready.
        //
        try {
            db = $cordovaSQLite.openDB({name:"nextflow.db",iosDatabaseLocation: 'Library'});
        } catch (error) {
            alert(error);
        }

        $cordovaSQLite.execute(db, 'CREATE TABLE IF NOT EXISTS Messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT)');


    });
})

.controller('NFController', ['$scope', '$cordovaSQLite', function($scope, $cordovaSQLite) {

    $scope.save = function(newMessage) {

        // execute INSERT statement with parameter
        $cordovaSQLite.execute(db, 'INSERT INTO Messages (message) VALUES (?)', [newMessage])
            .then(function(result) {
                $scope.statusMessage = "Message saved successful, cheers!";
            }, function(error) {
                $scope.statusMessage = "Error on saving: " + error.message;
            })

    }

    $scope.load = function() {

        // Execute SELECT statement to load message from database.
        $cordovaSQLite.execute(db, 'SELECT * FROM Messages ORDER BY id DESC')
            .then(
                function(res) {

                    if (res.rows.length > 0) {
                        $scope.newMessage = res.rows.item(0).message;
                        $scope.statusMessage = "Message loaded successful, cheers!";
                        console.log("SELECTED -> " + res.rows.item(0).message);

                    }
                },
                function(error) {
                    $scope.statusMessage = "Error on loading: " + error.message;
                }
            );
    }

}])
